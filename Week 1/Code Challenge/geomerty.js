//function to calculate beam
function calculateBeamVolume(p, l, t) {
  let luasAlas = p * l;
  let beamVolume = luasAlas * t;
  console.log("Volume balok adalah : " + beamVolume + " cm3.");
  return beamVolume;
}
let beamOne = calculateBeamVolume(5, 5, 10);
let beamTwo = calculateBeamVolume(10, 10, 20);
let resultAB = beamOne + beamTwo;
console.log("Volume Balok A + B adalah : " + resultAB + " cm3.");

//function to calculate cone
function calculateConeVolume(r, t) {
    let luasAlas = Math.PI *r **2
    let coneVolume = (1/3) * luasAlas * t;
    console.log("Volume kerucut adalah : " + coneVolume + " cm3.");
    return coneVolume;
}
let coneOne = calculateConeVolume(6, 10);
let coneTwo = calculateConeVolume(7, 20);
let resultCD = coneOne + coneTwo;
console.log("Volume kerucut A + B adalah : " + resultCD + " cm3.");
